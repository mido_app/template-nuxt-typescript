import Vue from 'vue'
import VueRouter, { Route } from 'vue-router'

/**
 * VueRouterプロパティの追加
 */
declare module "vue/types/vue" {
  interface Vue {
    $router: VueRouter
    $route: Route
  }
}
