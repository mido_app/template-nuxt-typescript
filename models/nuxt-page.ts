import { Store } from 'vuex'
import { Route } from 'vue-router'

/**
 * Nuxt.jsのコンテキスト
 */
export interface INuxtContext {
  params: any
  isClient: boolean
  isServer: boolean
  isStatic: boolean
  isDev: boolean
  isHHR: boolean
  route: Route
  store: Store<any>
  env: object
  query: object
  nuxtState: object
  req: Request
  res: Response
  redirect: (path: string) => void
  error: (params: { statusCode?: number, message?: string }) => void
  beforeNuxtRender: ({ Components, nuxtState }) => void
}

/**
 * ページへのヘッダー設定
 */
export interface INuxtHeaderOption {
  title?: string
  meta?: Array<object>
  script?: Array<object>
  link?: Array<object>
}

/**
 * ページへのトランジション設定
 * (注)
 * 他にもプロパティが存在するが必要になるまで記載しない
 * プロパティを追加する場合は以下を参照
 * https://ja.nuxtjs.org/api/pages-transition
 */
export interface INuxtTransitionObject {
  name?: string
}

/**
 * Nuxt.jsのページ
 */
export default interface INuxtPage {
  asyncData?: (context: INuxtContext) => void
  fetch?: (context: INuxtContext) => void
  head?: () => INuxtHeaderOption
  transition?: string | INuxtTransitionObject | ((to, from) => void)
  scrollToTop?: boolean
  validate?: ({params}) => boolean
}
