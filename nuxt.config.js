module.exports = {
  build: {
    vendor: [ 
      'nuxt-property-decorator'
    ]
  },
  modules: [
    '~/modules/typescript'
  ],
  css: [
    '@/assets/scss/app.scss'
  ]
}
